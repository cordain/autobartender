Feature: Automatic Bartender

    Project is intended to do task of mixing liquids provided in separate
    containers, considering propotions and order of selected liquids.
    Project takes use of variety of peristaltic pumps to push liquids
    from the source containers to desired user container.

    Product is customizable in terms of extending pumps capacity, as well
    as selection which pump (of which flow speed) is installed on each
    additional module. Product may measure weight of an liquid mixture to
    create feedback that improves precision of recreating mixtures.

    Product is accesssibe from either offline or online user interface.
    User can provide own recipes, monitor its preparation process and
    initiate it through both interfaces variants.
    
    Project provides following functionalities, in respect of future development:
    *   Recreating recipe on click-of-a-button
    *   Displaying recipes on on-board display
    *   Ability to change recipe using on-borad knob
    *   Providing user interface to configure list of offline recepies
    *   Ability to download recipe list from an online source (Smartphone app,
        server avaiable through Wi-Fi, etc.)
    *   Providing control and internal public data over network
    *   [TODO] Future product functionalities will be provided

    Rule: User will request to fill his or her container

        It is required to service mixture to the user, as filling it to
        his or her container

        Background:
            Given User put container on device plate
        
        Example: User wants vodka with coke
            Given User selects recipe that mixes vodka with coke in propotions 1:4
            When User confirms selection
            Then Container shall be filled with 40cl Vodka and 160cl Coke
        
        Example: User wants monte drink
            Given User selects monte recipe
            When User confirms selection
            Then Container shall be filled with 40cl Hazelnut Vodka and 120cl Milk
