Feature: Recipes handler

    Feature is responsible for recreating recipes for drink mixing.
    It uses the knowledge of module configuration to prepare conditions for
    correct mixture process, as well as filtering recipes for being avaiable
    in current configuration.