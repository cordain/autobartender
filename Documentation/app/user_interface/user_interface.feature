Feature: User interface

  Feature is responsible for drawing graphical UI, as well as handling
  input from remote device.

  Scenario Outline: Initialization

    Feature can be initialized with variable interface elements available
    Given User has turned on device
    * Device has <knob>
    * Device has <display>
    * Device has <remote>
    When Feature is initializing
    Then Feature shall <register_knob> status
    * Feature shall <register_display> status
    * Feature shall <register_remote> status

    Examples:
      | knob  | display | remote | register_knob | register_display | register_remote |
      | true  | false   | false  | true          | false            | false           |
      | true  | true    | false  | true          | true             | false           |
      | false | false   | true   | false         | false            | true            |
      | false | true    | true   | false         | true             | true            |
      | true  | true    | true   | true          | true             | true            |
      | true  | false   | true   | true          | false            | true            |

  Scenario Outline: Confirmation of selected recipe

    User has selected recipe which needs to be confirmed.
    Given User has selected recipe
    And Device has <confirmation> available
    When User uses <confirmation>
    Then Device shall begin preparation for recipe recreation

    Examples:
      | confirmation |
      | knob         |
      | remote       |

  Scenario Outline: Printing text on display

    Device will display text messages on display
    Given Device has display available
    When <message> needs to be displayed
    Then <message_value> is displayed on screen

    Examples:
      | message     | message_value |
      | welcome     | Automatic bartender welcomes you! |
      | book        | Recipe Book |
      | recipe1     | Recipe 1 |
      | recipe2     | Whiskey with cola |
      | warning     | Put container on the plate |
      | busy        | Processing... |
      | remote_info | Use remote access |
