Feature: Scale driver

  Feature is responsible for handling the weight measurement from device
  analog input.

  Rule: Configuration Variants

    Scale can be present as one of the measuring component:
      Not present
      1kg maximum weight
      2kg maximum weight

    Example Outline: Scale is contained in configuration
      Given Device has configuration table
      Then Configuration table shall contain <scale> configuration

      Examples:
        | scale       |
        | not present |
        | 1kg scale   |
        | 2kg scale   |

  Scenario Outline: Read weight measurement

    Scenario describes possible measurement values for readout
    Given <scale> is configured
    When Feature is asked for measurement values
    Then Feature shall return <calculated_weight> value in grams
    Examples:
      | scale       | calculated_weight |
      | not present | -1000             |
      | 1kg scale   | 500               |
      | 2kg scale   | 1200              |
