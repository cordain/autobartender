
use crate::platform::MotorLogicalChannel;
#[derive(Copy,Clone)]
enum LiquidSource{
    MainOuterLeft,
    MainInnerLeft,
    MainInnerRight,
    MainOuterRight,
    LIQUIDSOURCECOUNT
}

impl Into<MotorLogicalChannel> for LiquidSource {
    fn into(self) -> MotorLogicalChannel {
        match self {
            LiquidSource::MainOuterLeft => MotorLogicalChannel::CH0,
            LiquidSource::MainInnerLeft => MotorLogicalChannel::CH1,
            LiquidSource::MainInnerRight => MotorLogicalChannel::CH2,
            LiquidSource::MainOuterRight => MotorLogicalChannel::CH3,
            LiquidSource::LIQUIDSOURCECOUNT => panic!()
        }
    }
}

pub struct App {
    selected_liquid_source: LiquidSource,
    liquid_flows: bool,
    platform: super::platform::Platform
}

impl App {
    pub fn new(platform: super::platform::Platform) -> App {
        App {
            selected_liquid_source: LiquidSource::MainInnerLeft,
            liquid_flows: false,
            platform
        }
    }
    pub fn update(&mut self) {
        //update platform for fresh data
        self.platform.update();

        //update values for application to execute
        self.update_inputs();
        self.platform.change_motor_direction(true);

        //select channel to drive
        //if button was pressed, then activate flow on selected channel
        //if button was released, ther cut off liquid flow
        let pwm_channel = self.selected_liquid_source.into();
        if self.liquid_flows{
            self.platform.set_pump_channel(pwm_channel, 80);
        }
        else {
            self.platform.set_pump_channel(pwm_channel, 0);
        }
    }
    //helper function to calculate menu item
    fn jumps_to_new_item(&self,jumps: i8) -> LiquidSource{
        let max_sources = LiquidSource::LIQUIDSOURCECOUNT as u8;
        let source_idx = if jumps < 0 {
            let jumps = (jumps*(-1)) as u8 % max_sources;
            let current_source = {
                let mut current_source = self.selected_liquid_source as u8;
                if current_source < jumps{
                    current_source += max_sources;
                }
                current_source
            };
            current_source - jumps
        }
        else {
            (self.selected_liquid_source as u8 + (jumps as u8)) % max_sources
        };
        match source_idx {
            0 => LiquidSource::MainOuterLeft,
            1 => LiquidSource::MainInnerLeft,
            2 => LiquidSource::MainInnerRight,
            3 => LiquidSource::MainOuterRight,
            _ => panic!()
        }
    }
    pub fn update_inputs(&mut self){
        let (menu_jumps,liquid_flow_state) = self.platform.get_knob_data();
        self.selected_liquid_source = self.jumps_to_new_item(menu_jumps);
        self.liquid_flows = liquid_flow_state;
    }
}
