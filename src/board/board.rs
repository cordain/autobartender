use core::mem::replace;

use hal::device::{NVIC, I2C3};
use hal::i2c::{I2c, Config};
use stm32l4xx_hal as hal;
use hal::prelude::{_stm32l4_hal_FlashExt,_embedded_hal_PwmPin, _embedded_hal_blocking_delay_DelayMs};
use hal::gpio::*;
use hal::stm32::TIM1;
use hal::pwm::*;
use hal::delay::Delay;
use hal::pwr::PwrExt;
use hal::rcc::RccExt;
use hal::time::Hertz;
use hal::pac::interrupt;

struct Board {
    //test diode
    led: Option<gpiob::PB3<Output<PushPull>>>,
    //Pump driving peripherals
    direction: Option<gpioa::PA12<Output<PushPull>>>,
    pwm00: Option<Pwm<TIM1,C1>>,
    pwm01: Option<Pwm<TIM1,C2>>,
    pwm02: Option<Pwm<TIM1,C3>>,
    pwm03: Option<Pwm<TIM1,C4>>,
    //pwm10: Option<Pwm<TIM2,C1>,
    //pwm11: Option<Pwm<TIM2,C2>,
    
    //Encoder peripherals
    //encoder: Option<LowPowerTimer<LPTIM1>>,
    enc_in_1: Option<gpiob::PB5<Input<PullUp>>>,
    enc_in_2: Option<gpiob::PB7<Input<PullUp>>>,
    enc_swt: Option<gpiob::PB6<Input<PullUp>>>,
    
    //General purpose i2c bus (primarily for screen)
    gpi2c: Option<I2c<I2C3,(gpioa::PA7<Alternate<OpenDrain,4>>,gpiob::PB4<Alternate<OpenDrain,4>>)>>,

    //delay
    delay: Option<Delay>,
}

const ENCODER_QUEUE_SIZE: usize = 20;
struct EncoderDataQueue{
    data: [(bool,bool); 20],
    queue_first: usize,
    queue_last: usize,
    queue_empty: bool
}

static mut ENCODER_QUEUE: EncoderDataQueue = EncoderDataQueue{
    //utility for encoder
    data: [(false,false); ENCODER_QUEUE_SIZE],
    queue_first: 0,
    queue_last: 0,
    queue_empty: true
};

static mut BOARD: Board = Board{
    led: None,
    //Pump driving peripherals
    direction: None,
    pwm00: None,
    pwm01: None,
    pwm02: None,
    pwm03: None,
    
    //Encoder peripherals
    enc_in_1: None,
    enc_in_2: None,
    enc_swt: None,
    
    //General purpose i2c bus (primarily for screen)
    gpi2c: None,

    //delay
    delay: None,


};

impl Board{
    fn init(&mut self){
        //take all peripherals on the uP
        let all_periph = hal::stm32::Peripherals::take().unwrap();
        let core_periph = hal::stm32::CorePeripherals::take().unwrap();
        //gather mutable access to Reset and Clock Controler
        let mut reset_and_clock_control = all_periph.RCC.constrain();
        //gather mutable access to FLASH storage
        let mut flash = all_periph.FLASH.constrain();
        //gather mutable access to Power Manager
        let mut pwr = all_periph.PWR.constrain(&mut reset_and_clock_control.apb1r1);
        //setup clock
        let clocks = reset_and_clock_control.cfgr
            .sysclk(Hertz::MHz(64))
            .freeze(&mut flash.acr, &mut pwr);
        //create delay functionality
        self.delay = Some(Delay::new(core_periph.SYST, clocks));
        let mut syscfg = all_periph.SYSCFG;
        let mut exti = all_periph.EXTI;
        let mut i2c3 = all_periph.I2C3;

        let mut gpioa = all_periph.GPIOA.split(&mut reset_and_clock_control.ahb2);
        let mut gpiob = all_periph.GPIOB.split(&mut reset_and_clock_control.ahb2);
        //let mut encoder = LowPowerTimer::lptim1(all_periph.LPTIM1,encoder_config,&mut reset_and_clock_control.apb1r1,)
        let pwm0_pins = ( 
            gpioa.pa8.into_alternate::<1_u8>(&mut gpioa.moder,&mut gpioa.otyper,&mut gpioa.afrh),
            gpioa.pa9.into_alternate::<1_u8>(&mut gpioa.moder,&mut gpioa.otyper,&mut gpioa.afrh),
            gpioa.pa10.into_alternate::<1_u8>(&mut gpioa.moder,&mut gpioa.otyper,&mut gpioa.afrh),
            gpioa.pa11.into_alternate::<1_u8>(&mut gpioa.moder,&mut gpioa.otyper,&mut gpioa.afrh)
        );
        
        self.direction = Some(gpioa.pa12.into_push_pull_output(&mut gpioa.moder,&mut gpioa.otyper));

        self.led = Some(gpiob.pb3.into_push_pull_output(&mut gpiob.moder,&mut gpiob.otyper));

        let (pwm00,pwm01,pwm02,pwm03) = all_periph.TIM1.pwm(
            pwm0_pins, 
            Hertz::Hz(10_000u32), 
            clocks, 
            &mut reset_and_clock_control.apb2
        );
        let mut enc_in_1 = gpiob.pb5.into_pull_up_input(&mut gpiob.moder,&mut gpiob.pupdr);
        enc_in_1.enable_interrupt(&mut exti);
        enc_in_1.trigger_on_edge(&mut exti,Edge::RisingFalling);
        enc_in_1.make_interrupt_source(&mut syscfg,&mut reset_and_clock_control.apb2);
        let mut enc_in_2 = gpiob.pb7.into_pull_up_input(&mut gpiob.moder,&mut gpiob.pupdr);
        enc_in_2.enable_interrupt(&mut exti);
        enc_in_2.trigger_on_edge(&mut exti,Edge::Rising);
        enc_in_2.make_interrupt_source(&mut syscfg,&mut reset_and_clock_control.apb2);
        let enc_swt = gpiob.pb6.into_pull_up_input(&mut gpiob.moder,&mut gpiob.pupdr);
        //enc_swt.enable_interrupt(&mut exti);
        //enc_swt.trigger_on_edge(&mut exti,Edge::Rising);
        //enc_swt.make_interrupt_source(&mut syscfg,&mut reset_and_clock_control.apb2);
        
        let gpi2c = I2c::i2c3(
            i2c3,
            (
                gpioa.pa7.into_alternate_open_drain::<4u8>(&mut gpioa.moder,&mut gpioa.otyper,&mut gpioa.afrl),
                gpiob.pb4.into_alternate_open_drain::<4u8>(&mut gpiob.moder,&mut gpiob.otyper,&mut gpiob.afrl)
            ),
            Config::new(Hertz::kHz(400),clocks),
            &mut reset_and_clock_control.apb1r1
        );

        self.enc_in_1 = Some(enc_in_1);
        self.enc_in_2 = Some(enc_in_2);
        self.enc_swt = Some(enc_swt);
        self.pwm00 = Some(pwm00);
        self.pwm01 = Some(pwm01);
        self.pwm02 = Some(pwm02);
        self.pwm03 = Some(pwm03);
        self.gpi2c = Some(gpi2c);

        unsafe{NVIC::unmask(interrupt::EXTI9_5)};
        //hprintln!("BOARD INITIALIZED");
    }
    pub fn take_peripheral<T>(&mut self) -> T{
        unimplemented!(); //maybe macro instead?
        /*let taken = match T{
            gpioa::PA12<Output<PushPull>> => replace(&mut self.direction, None)
        };
        */
    }
}

impl EncoderDataQueue{
    fn add_to_queue(&mut self, item: (bool,bool)) -> Result<(),u32>{
        if !self.queue_empty && (self.queue_last == self.queue_first){
            Err(0x23)
        }
        else{
            self.data[self.queue_last] = item;
            self.queue_last += 1;
            self.queue_last %= ENCODER_QUEUE_SIZE;
            self.queue_empty = false;
            Ok(())
        }

    }
    fn take_from_queue(&mut self) -> Option<(bool,bool)>{
        if self.queue_empty{
            None
        }
        else{
            let item = self.data[self.queue_first];
            self.queue_first += 1;
            self.queue_first %= ENCODER_QUEUE_SIZE;
            self.queue_empty = self.queue_first == self.queue_last;
            Some(item)
        }
    }
}

pub fn take_encoder_queue_record() -> Option<(bool,bool)>{
    unsafe {ENCODER_QUEUE.take_from_queue()}
}
pub fn take_gpi2c() -> I2c<I2C3,(gpioa::PA7<Alternate<OpenDrain,4>>,gpiob::PB4<Alternate<OpenDrain,4>>)>{
    let taken = unsafe {replace(&mut BOARD.gpi2c, None)};
    taken.unwrap()
}
pub fn take_enc_swt() -> gpiob::PB6<Input<PullUp>>{
    let taken = unsafe {replace(&mut BOARD.enc_swt, None)};
    taken.unwrap()
}
pub fn take_direction_pin() -> gpioa::PA12<Output<PushPull>>{
    let taken = unsafe {replace(&mut BOARD.direction, None)};
    taken.unwrap()
}
pub fn take_pwm0_channel0() -> Pwm<TIM1,C1>{
    let taken = unsafe {replace(&mut BOARD.pwm00, None)};
    taken.unwrap()
}
pub fn take_pwm0_channel1() -> Pwm<TIM1,C2>{
    let taken = unsafe {replace(&mut BOARD.pwm01, None)};
    taken.unwrap()
}
pub fn take_pwm0_channel2() -> Pwm<TIM1,C3>{
    let taken = unsafe {replace(&mut BOARD.pwm02, None)};
    taken.unwrap()
}
pub fn take_pwm0_channel3() -> Pwm<TIM1,C4>{
    let taken = unsafe {replace(&mut BOARD.pwm03, None)};
    taken.unwrap()
}
pub fn init() {unsafe{BOARD.init()}}

#[interrupt]
fn EXTI9_5(){
    unsafe{
        let freeze_encoder = (
            BOARD.enc_in_1.as_ref().unwrap().is_high(),
            BOARD.enc_in_2.as_ref().unwrap().is_high(),
        );
        ENCODER_QUEUE.add_to_queue(freeze_encoder).unwrap();
        BOARD.enc_in_1.as_mut().unwrap().clear_interrupt_pending_bit();
        BOARD.enc_in_2.as_mut().unwrap().clear_interrupt_pending_bit();
    }
}
