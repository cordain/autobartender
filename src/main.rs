#![no_std]
#![no_main]

// main architecture design modules breakdown
#[path = "app/app.rs"]
mod app{
    //what makes the feature callable
    pub trait callable{
        pub fn init();
        pub fn update();
    }
    //features associated with application layer
    mod menu;
    mod recipe_handler;
    mod feedback_assist;
    mod pump_orchestrator;
    //feature of hardware abstraction platform
    #[path = "../platform/platform.rs"]
    mod platform{
        mod peristaltic_motor;
        mod encoder_knob;
        mod monochrome_display;
        mod serial_bus;
        mod wheatstone_bridge;
        #[path ="../board/board.rs"]
        mod board{

        }
    }
}
//  mod app_driver;
mod platform;

// use for defining entry point as any function name
use cortex_m_rt::entry;

// pick a panicking behavior
use panic_halt as _; // you can put a breakpoint on `rust_begin_unwind` to catch panics


#[entry]
fn main() -> ! {
    let platform = platform::Platform::new();
    //let mut app_driver = app_driver::AppDriver::new();
    let mut app = app::App::new(platform);
    loop {
        app.update();
    }
}
