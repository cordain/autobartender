use stm32l4xx_hal::gpio::{gpiob, Input, PullUp};

pub struct Knob{
    switch: gpiob::PB6<Input<PullUp>>,
    left_counts: u8,
    right_counts: u8,
}

impl Knob{
    pub fn new() -> Knob{
        Knob {
            switch: crate::platform::board::take_enc_swt(),
            left_counts: 0,
            right_counts: 0,
        }
    }

    pub fn update(&mut self){
        let mut encoder_queue_record = crate::platform::board::take_encoder_queue_record();
        while let Some(data) = encoder_queue_record {
            let (left_state, right_state) = data;
            //hprintln!("{:?}", data);
            if !left_state {
                self.right_counts += 1;
            }
            if !right_state{
                self.left_counts += 1;
            }
            encoder_queue_record = crate::platform::board::take_encoder_queue_record();
        }
    } 
    pub fn calculate_ticks(&mut self) -> i8{
        let tics = self.right_counts as i8 - self.left_counts as i8;
        self.right_counts = 0;
        self.left_counts = 0;
        tics
    }
    pub fn get_pressed(&self) -> bool {self.switch.is_low()}
}
