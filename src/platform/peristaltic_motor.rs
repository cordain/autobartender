use stm32l4xx_hal::{pwm::*, device::TIM1,gpio::{gpioa::PA12, Output, PushPull}, prelude::_embedded_hal_PwmPin};

#[derive(Copy,Clone)]
pub enum MotorLogicalChannel {
    CH0,
    CH1,
    CH2,
    CH3,
    CH4,
    CH5
}

pub enum MotorType {
    NoMotor,
    PerGeneric,
    PerGrothen500,
    PerGrothen100,
    DcGeneric
}

pub struct Motor{
    motor_outer_left: Pwm<TIM1,C1>,
    motor_inner_left: Pwm<TIM1,C2>,
    motor_inner_right: Pwm<TIM1,C3>,
    motor_outer_right: Pwm<TIM1,C4>,
    direction_pin: PA12<Output<PushPull>>,
    pwm_values: [u8;4],
    direction: bool
}

impl Motor {
    pub fn new(
        motor_outer_left: Pwm<TIM1,C1>,
        motor_inner_left: Pwm<TIM1,C2>,
        motor_inner_right: Pwm<TIM1,C3>,
        motor_outer_right: Pwm<TIM1,C4>,
        direction_pin: PA12<Output<PushPull>>
    ) -> Motor {
        Motor {
            motor_outer_left,
            motor_inner_left,
            motor_inner_right,
            motor_outer_right,
            direction_pin,
            pwm_values: [0;4],
            direction: false
        }
    }

    pub fn set_motor_channel(&mut self, channel: MotorLogicalChannel, pwm_value: u8){
        let ch = channel as usize;
        if self.pwm_values[ch] != pwm_value {self.pwm_values[ch] = pwm_value;}
        
    }
    pub fn change_direction(&mut self, direction: bool){
        if self.direction != direction {self.direction = direction;}
    }
    pub fn update(&mut self){
        //setup pin according to set direction
        if self.direction {self.direction_pin.set_high()} else {self.direction_pin.set_low()}

        self.motor_outer_left.enable();
        self.motor_inner_left.enable();
        self.motor_inner_right.enable();
        self.motor_outer_right.enable();
        //update pwm values for all channels
        self.motor_outer_left.set_duty((self.motor_outer_left.get_max_duty()/100) * (self.pwm_values[0] as u16));
        self.motor_inner_left.set_duty((self.motor_inner_left.get_max_duty()/100) * (self.pwm_values[1] as u16));
        self.motor_inner_right.set_duty((self.motor_inner_right.get_max_duty()/100) * (self.pwm_values[2] as u16));
        self.motor_outer_right.set_duty((self.motor_outer_right.get_max_duty()/100) * (self.pwm_values[3] as u16));
    }
}
