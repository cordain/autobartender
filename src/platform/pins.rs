use stm32l4xx_hal::gpio::{gpioa,gpiob,gpioc,gpiod,gpioe};
use stm32l4xx_hal::i2c::*;
use stm32l4xx_hal::stm32::I2C3;
use stm32l4xx_hal::gpio::*;

//pub type RCC_CK_IN = gpioa::PA0;
pub type PUMP_PWM_11 = gpioa::PA1<Alternate<OTYPE, 1>>;
pub type VCP_TX = gpioa::PA2;
pub type PUMP_PWM_10 = gpioa::PA5<Alternate<OTYPE, 1>>;
pub type GPI2C_SCL = gpioa::PA7<Alternate<OpenDrain, 4>>;
pub type PUMP_PWM_00 = gpioa::PA8<Alternate<OTYPE, 1>>;
pub type PUMP_PWM_01 = gpioa::PA9<Alternate<OTYPE, 1>>;
pub type PUMP_PWM_02 = gpioa::PA10<Alternate<OTYPE, 1>>;
pub type PUMP_PWM_03 = gpioa::PA11<Alternate<OTYPE, 1>>;
pub type DIR_SWITCH = gpioa::PA12<hal::gpio::Output<hal::gpio::PushPull>>;
pub type SWDIO = gpioa::PA13;
pub type SWCLK = gpioa::PA14;
pub type VCP_RX = gpioa::PA15;

pub type LED = gpiob::PB3;
pub type GPI2C_SDA = gpiob::PB4<Alternate<OpenDrain, 4>>;
pub type ENC_IN0 = gpiob::PB5;
pub type ENC_IN1 = gpiob::PB7;

pub type RCC_OSC32_IN = gpioc::PC14;
pub type RCC_OSC32_OUT = gpioc::PC15;
