mod board;

mod motor;
//mod screen;
mod knob;
//mod bridge;

use board::*;
use motor::Motor;
pub use motor::MotorLogicalChannel;
use knob::Knob;

pub struct Platform{
    knob: Knob,
    motor: Motor
}


impl Platform {
    pub fn new() -> Platform {
        board::init();
        let pwm_channels = (
            take_pwm0_channel0(),
            take_pwm0_channel1(),
            take_pwm0_channel2(),
            take_pwm0_channel3(),
        );
        let direction_pin = take_direction_pin();
        let motor = Motor::new(
            pwm_channels.0,
            pwm_channels.1,
            pwm_channels.2,
            pwm_channels.3,
            direction_pin
        );

        let knob = Knob::new();
                
        Platform {motor, knob}
    }
    pub fn get_knob_data(&mut self) -> (i8,bool){
        (self.knob.calculate_ticks(), self.knob.get_pressed())
    }
    pub fn set_pump_channel(&mut self,channel: MotorLogicalChannel, speed_percent: u8){
        self.motor.set_motor_channel(channel,speed_percent);
    }
    pub fn update(&mut self) {
        self.knob.update();
        self.motor.update();
    }
    pub fn change_motor_direction(&mut self, direction: bool){
        self.motor.change_direction(direction);
    }
}
